import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import TextField from 'material-ui/TextField';
import { RadioButton, RadioButtonGroup } from 'material-ui/RadioButton';
import Checkbox from 'material-ui/Checkbox';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import { connect } from 'react-redux'

const validate = values => {
    const errors = { exist : false }
    const requiredFields = [ 'name', 'surname'];
    requiredFields.forEach(field => {
      if (!values[ field ]) {
        errors[ field ] = 'Required'
        errors.exist = true
      }
    })
    if (!values.name ) {
      errors.name = 'Name should be longer'
      errors.exist = true
    }
    if (!values.surname ) {
      errors.surname = 'surname should be longer'
      errors.exist = true
    }
    return errors
  }

class AddAgent extends Component {
  renderTextField = ({ input, label, meta: { touched, error }, ...custom }) => {
    return (
    <TextField hintText={label}
      floatingLabelText={label}
      errorText={touched && error}
      {...input}
      {...custom}
    />)
  }
 renderCheckbox = ({ input, label }) => (
    <Checkbox label={label}
      checked={input.value ? true : false}
      onCheck={input.onChange}/>
  )

  renderRadioGroup = ({ input, ...rest }) => (
    <RadioButtonGroup {...input} {...rest}
      valueSelected={input.value}
      onChange={(event, value) => input.onChange(value)}/>
  )

  renderSelectField = ({ input, label, meta: { touched, error }, children, ...custom }) => (
    <SelectField
      floatingLabelText={label}
      errorText={touched && error}
      {...input}
      onChange={(event, index, value) => input.onChange(value)}
      children={children}
      {...custom}/>
  )
  render() {
  const { handleSubmit, pristine, reset, submitting, agents } = this.props;
  return (
    <form onSubmit={ handleSubmit }>
      <div>
        <Field name="name"  component={ this.renderTextField } label="First name"/>
      </div>
      <div>
        <Field name="surname" component={ this.renderTextField } label="Second Name"/>
      </div>
    </form>
  )
  }
}
AddAgent = reduxForm({
  form: 'AddAgent',  // a unique identifier for this form
  validate
})(AddAgent)

AddAgent = connect(
  state => ({
    initialValues: state.Data.dialog.editedItem 
  })
)(AddAgent)
export default AddAgent;