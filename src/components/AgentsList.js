import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { List } from 'material-ui/List';
import Subheader from 'material-ui/Subheader';
import {grey400, darkBlack, lightBlack} from 'material-ui/styles/colors';
import AgentItem from './AgentItem';
import FlatButton from 'material-ui/FlatButton';

export default class AgentsList extends Component {
  render() {
    const { transactions, agents, actions, dialog, } = this.props;
    var ts = [];
      const tmp = agents.map(function (el, i) {
        ts = transactions.filter(function (els) {
          return els.agentId == el.id
        })
        return (
          <div key={i}>
            <AgentItem
              item={el}
              actions={ actions }
              index={i}
              transaction={ts}
              />
          </div>
        )
      })
    return (
      <div className="transactions_container">
          <List>
            <Subheader className="transactions_subheader">
              <div className="transactions_title">
                <h3>All agents</h3>
                <FlatButton onClick={()=>{ actions.openDialog({addTransaction:false,addAgent:true,dialogType: 'add'}) }} label="Add agent" primary={true}/>
              </div>
            </Subheader>
            {tmp}
          </List>
      </div>
    );
  }
}
