import React, {Component} from 'react';
import FontIcon from 'material-ui/FontIcon';
import {BottomNavigation, BottomNavigationItem} from 'material-ui/BottomNavigation';
import Paper from 'material-ui/Paper';
import IconLocationOn from 'material-ui/svg-icons/action/account-circle';
import ActionHome from 'material-ui/svg-icons/action/euro-symbol';
import 	{
   Link
  } from 'react-router-dom'

const recentsIcon = <ActionHome/>;
const nearbyIcon = <IconLocationOn />;

/**
 * A simple example of `BottomNavigation`, with three labels and icons
 * provided. The selected `BottomNavigationItem` is determined by application
 * state (for instance, by the URL).
 */
class BottomNav extends Component {

  render() {

    const { navigate} = this.props
    return (
    <div className="nav">
      <Paper zDepth={5}>
        <BottomNavigation selectedIndex={navigate}>
        <Link className="link" to='/transactions'>
            <BottomNavigationItem
              label="Transactions"
              icon={recentsIcon}
            />
        </Link>
        <Link className="link" to='/agents'>
            <BottomNavigationItem
              label="Agents"
              icon={nearbyIcon}
            />
        </Link>
        </BottomNavigation>
      </Paper>
      </div>
    );
  }
}

export default BottomNav;