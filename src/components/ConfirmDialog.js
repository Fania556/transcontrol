import React, { Component } from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';

export default class ConfirmDialog extends Component {
  render() {
    const { action, title, dialog, closeDialog } = this.props;
  

    const actions = [
      <FlatButton
        label="Cancel"
        primary={true}
        onTouchTap={()=>{console.log(dialog.submit); closeDialog(); }}
      />,
      <FlatButton
        label="Submit"
        primary={true}
        keyboardFocused={true}
        onTouchTap={ ()=>{ action(dialog.itemId) } }
      />,
    ];
    return (
      <div>
        <Dialog
          title={ 'Please confirm deletion the ' + title }
          actions={ actions }
          modal={ false }
          open={  dialog.submit != undefined ? dialog.submit: false  }
          onRequestClose={ closeDialog }
        >
          Are you sure you want to delete the item?
        </Dialog>
      </div>
    );
  }
}