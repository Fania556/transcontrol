import React, { Component } from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';


export default class FormDialog extends Component {
  
  render() {
    const { actions, title, dialog, closeDialog, formData, agents, Form } = this.props;
    const getCurrentForm = () =>{
      if(dialog.addTransaction) return formData.AddTransaction;
      else if(dialog.editedItem.cost) return formData.AddTransaction;
      else return formData.AddAgent;
      
    } 
    const buttons = [
      <FlatButton
        label="Cancel"
        primary={true}
        onTouchTap={ closeDialog }
      />,
      <FlatButton
        label="Submit"
        primary={true}
        disabled = { formData.AddTransaction != undefined  ? formData.AddTransaction.syncErrors.exist  : formData.AddAgent != undefined ?formData.AddAgent.syncErrors.exist : false }
        keyboardFocused={true}
        onTouchTap={ ()=>{ dialog.addAgent || dialog.addTransaction ? dialog.addTransaction ? actions[1](getCurrentForm().values) : actions[0](getCurrentForm().values) : actions[2](getCurrentForm().values,dialog.itemId) } }
      />,
    ];
    return (
      <div>
        <Dialog
          autoScrollBodyContent = {true}
          title={ 'Add ' + title }
          actions={ buttons }
          modal={ true }
          open={ dialog.addTransaction || dialog.edit || dialog.addAgent}
          onRequestClose={ closeDialog }
        >
        
          <Form agents={agents} style="width: 100%" />
        </Dialog>
      </div>
    );
  }
}