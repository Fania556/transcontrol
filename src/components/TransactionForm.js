import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import TextField from 'material-ui/TextField';
import { RadioButton, RadioButtonGroup } from 'material-ui/RadioButton';
import Checkbox from 'material-ui/Checkbox';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import { connect } from 'react-redux'

const validate = values => {
    const errors = { exist : false }
    const requiredFields = [ 'cost', 'agentId']
    requiredFields.forEach(field => {
      if (!values[ field ]) {
        errors[ field ] = 'Required'
        errors.exist = true
      }
    })
    if (values.cost && !/^(-)?\d+$/i.test(values.cost)) {
      errors.cost = 'Cost must includes only a decimals'
      errors.exist = true
    }
    return errors
  }

class AddTransaction extends Component {
  renderTextField = ({ input, label, meta: { touched, error }, ...custom }) => {
    return (
    <TextField hintText={label}
      floatingLabelText={label}
      errorText={touched && error}
      {...input}
      {...custom}
    />)
  }
 renderCheckbox = ({ input, label }) => (
    <Checkbox label={label}
      checked={input.value ? true : false}
      onCheck={input.onChange}/>
  )

  renderRadioGroup = ({ input, ...rest }) => (
    <RadioButtonGroup {...input} {...rest}
      valueSelected={input.value}
      onChange={(event, value) => input.onChange(value)}/>
  )

  renderSelectField = ({ input, label, meta: { touched, error }, children, ...custom }) => (
    <SelectField
      floatingLabelText={label}
      errorText={touched && error}
      {...input}
      onChange={(event, index, value) => input.onChange(value)}
      children={children}
      {...custom}/>
  )
  render() {
  const { handleSubmit, pristine, reset, submitting, agents } = this.props;
  const agentsSelect = agents.map((el,i)=>(<MenuItem key={i} value={el.id} primaryText={el.name+' '+el.surname}/>))
  return (
    <form onSubmit={ handleSubmit }>
      <div>
        <Field name="cost" value="2" component={ this.renderTextField } label="Count of money"/>
      </div>
      <div>
        <Field name="agentId" component={ this.renderSelectField } label="Agent's id">
          {agentsSelect}
        </Field>
      </div>
    </form>
  )
  }
}
AddTransaction = reduxForm({
  form: 'AddTransaction',  // a unique identifier for this form
  validate
})(AddTransaction)

AddTransaction = connect(
  state => ({
    initialValues: state.Data.dialog.editedItem 
  })
)(AddTransaction)
export default AddTransaction;