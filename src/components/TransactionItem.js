import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {List, ListItem} from 'material-ui/List';
import Divider from 'material-ui/Divider';
import Subheader from 'material-ui/Subheader';
import Avatar from 'material-ui/Avatar';
import { gray400, cyan100, darkBlack, lightBlack } from 'material-ui/styles/colors';
import IconButton from 'material-ui/IconButton';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';

export default class Transaction extends Component {

  render() {
    const { item, actions, index, agent, dialog } = this.props;
    const iconButtonElement = (
      <IconButton
        touch={true}
        tooltip="more"
        tooltipPosition="bottom-left"
      >
        <MoreVertIcon color={ gray400 } />
      </IconButton>
    );
    const rightIconMenu = (
      <IconMenu iconButtonElement={iconButtonElement}
      anchorOrigin={{horizontal: 'left', vertical: 'top'}}
      targetOrigin={{horizontal: 'left', vertical: 'top'}}
      >
        <MenuItem onClick={ ()=>{ actions.openDialog({index:item.id,data:{cost:item.cost,agentId:item.agentId}, dialogType: 'edit'}) } }>Edit</MenuItem>
        <MenuItem onClick={ ()=>{ actions.openDialog({index:item.id, dialogType: 'delete'}) } }>Delete</MenuItem>
      </IconMenu>
    );
    return (
      <div >
      <Divider/>
      <ListItem
        rightIconButton={rightIconMenu}
        primaryText= {  '$'+item.cost }
        secondaryTextLines={2}
        secondaryText={
        <p>
        <span style={ {color: gray400 } }>{
          'Date: '+ (new Date(item.date)).toLocaleString('en-US')
        }</span><br/>
        <span style={ {color: gray400} }>
          {'Agent: '+ (agent == undefined ? 'agent deleted' : agent.name +' '+ agent.surname)}
        </span>
        </p>
      }
      />
      </div>
      
    )
  }
}
