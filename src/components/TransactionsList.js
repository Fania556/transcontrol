import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {List} from 'material-ui/List';
import Subheader from 'material-ui/Subheader';
import {grey400, darkBlack, lightBlack} from 'material-ui/styles/colors';
import Transaction from './TransactionItem';
import FlatButton from 'material-ui/FlatButton';

export default class Transactions extends Component {
  render() {
    const { transactions, actions, agents, dialog } = this.props;
    var TotalCount=0,TotalCost = 0;
    const tmp = transactions.map(function (el, i) {
      TotalCount++;
      TotalCost+=el.cost;
      return (
        <div key={i}>
          <Transaction
            item={el}
            actions={actions}
            index={i}
            agent={ agents.find(function(item){ return item.id == el.agentId })}
            />
        </div>
      )
    })
    return (
      <div className="transactions_container">
          <List>
            <Subheader className="transactions_subheader">
              <div className="transactions_title">
                <h3>All transactions</h3>
                <FlatButton onClick={()=>{ actions.openDialog({addTransaction:true,addAgent:false,dialogType: 'add'}) }} label="Add Transaction" primary={true}/>
              </div>
              <div className="transactions_info">
                <div> Total  count: {TotalCount} </div>
                <div> Total  cost: {TotalCost} </div>
              </div>
            </Subheader>
            {tmp}
          </List>
      </div>
    );
  }
}
