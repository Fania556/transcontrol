import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AgentsList from '../components/AgentsList';
import BottomNav from '../components/BottomNav';
import { 
  fetchAllTransactions, 
  fetchAllAgents, 
  deleteAgentInDB,
  addAgentInDB,
  updateAgentInDB,
  addTransactionInDB,
  openDialog, 
  closeDialog,
  navigate
  } from '../ducks/Data';
import ConfirmDialog from '../components/ConfirmDialog';
import FormDialog from '../components/FormDialog';
import TransactionForm from '../components/TransactionForm';
import AgentForm from '../components/AgentForm';

class AgentsContainer extends Component {
  componentWillMount(){
    this.props.fetchAllAgents();
    this.props.fetchAllTransactions();
    this.props.navigate(1);
}
  render() {
    const { 
      transactions, 
      agents, 
      fetchAllAgents, 
      fetchAllTransactions, 
      deleteAgentInDB, 
      closeDialog, 
      openDialog, 
      dialog, 
      formData,
      addAgentInDB,
      addTransactionInDB,
      updateAgentInDB,
      navigate,
      view
    } = this.props;
    const actions = {
      openDialog
    }
    const actionsInDB = [
      addAgentInDB,
      addTransactionInDB,
      updateAgentInDB
    ]
    return (
      <div> 
            <AgentsList actions={ actions } dialog={ dialog }  agents={ agents } transactions={ transactions }/>
            <ConfirmDialog dialog={ dialog } action={ deleteAgentInDB } title="agent" closeDialog={ closeDialog } />
            <FormDialog Form= { dialog.addAgent || dialog.edit ? AgentForm : TransactionForm } formData={ formData } actions={ actionsInDB } title="transaction" dialog={ dialog } closeDialog={ closeDialog } agents={agents} />
            <BottomNav navigate = { view.navigate }/>

      </div>
    )
  }
}

const mapStateToProps = (state) => {return {transactions: state.Data.transactions, agents: state.Data.agents, dialog: state.Data.dialog, formData: state.form, view: state.Data.view }};

export default connect(mapStateToProps, {
  fetchAllTransactions, 
  fetchAllAgents, 
  deleteAgentInDB, 
  closeDialog, 
  openDialog, 
  addAgentInDB, 
  addTransactionInDB,
  updateAgentInDB,
  navigate })(AgentsContainer)