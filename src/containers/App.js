import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {createStore} from 'redux';
import {connect} from 'react-redux';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { bindActionCreators } from 'redux';
import { deleteItem, renameItem, addItem } from '../ducks/Transactions';
import TransactionsContainer from './TransactionsContainer';
import AgentsContainer from './AgentsContainer';
import {
  BrowserRouter as Router,
  Route,
  Link,
	Redirect
} from 'react-router-dom'

export default class App extends Component {
  render() {
      return (
				<MuiThemeProvider>
          <Router>
						<div className="container">
							<Route exact path="/" render={()=>(
								<Redirect to="/transactions"/>
							)}/>
							<Route path="/transactions" render={()=>(
								<TransactionsContainer/>
							)}/>
							<Route path="/agents" render={()=>(
								<AgentsContainer/>
							)}/>
						</div>
					</Router>
				</MuiThemeProvider>
		);
}
}


