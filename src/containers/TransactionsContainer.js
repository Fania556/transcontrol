import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {connect} from 'react-redux';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import TransactionsList from '../components/TransactionsList';
import {deleteItem,renameItem,addItem} from '../ducks/Transactions';
import { bindActionCreators } from 'redux';
import BottomNav from '../components/BottomNav';
import { 
  fetchAllTransactions, 
  fetchAllAgents, 
  openDialog, 
  closeDialog, 
  deleteTransactionInDB, 
  addTransactionInDB, 
  errorDialog, 
  updateTransactionInDB,
  addAgentInDB,
  navigate
  }
  from '../ducks/Data';
import ConfirmDialog from '../components/ConfirmDialog';
import FormDialog from '../components/FormDialog';
import TransactionForm from '../components/TransactionForm';

class TransactionsContainer extends Component {
  componentWillMount(){
    this.props.fetchAllAgents();
    this.props.fetchAllTransactions();
    this.props.navigate(0);
  }
  render() {
    const { 
       transactions,
       deleteItem,
       renameItem,
       addItem,
       agents,
       fetchAllAgents,
       fetchAllTransactions,
       deleteTransactionInDB,
       addTransactionInDB,
       openDialog,
       closeDialog,
       formData,
       errorDialog,
       addAgentInDB,
       updateTransactionInDB,
       dialog,view } = this.props;
    const actions = {
      deleteItem,
      renameItem,
      deleteTransactionInDB,
      openDialog, 
      closeDialog,
      errorDialog,
    }
    const actionsInDB = [
      addAgentInDB,
      addTransactionInDB,
      updateTransactionInDB
    ]
    return (
      <div>
            <TransactionsList dialog={ dialog } actions={ actions } agents={ agents } transactions={ transactions }/>
            <ConfirmDialog dialog={ dialog } action={ deleteTransactionInDB } title="transaction" closeDialog={ closeDialog } />
            <FormDialog Form= { TransactionForm } formData={ formData } actions={ actionsInDB } title="transaction" dialog={ dialog } closeDialog={ closeDialog } agents={agents} />
            <BottomNav navigate={ view.navigate }/>
      </div>
    )
  }
}

const mapStateToProps = (state) => {return {transactions: state.Data.transactions, agents: state.Data.agents, dialog: state.Data.dialog, formData: state.form, view: state.Data.view }};


export default connect(mapStateToProps, {
  deleteItem, 
  renameItem, 
  addItem, 
  fetchAllTransactions, 
  fetchAllAgents,
  deleteTransactionInDB,
  openDialog, 
  closeDialog,
  addTransactionInDB,
  updateTransactionInDB,
  addAgentInDB,
  navigate
})(TransactionsContainer)