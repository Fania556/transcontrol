const DELETE_ITEM = 'ducks/agents/DELETE_ITEM';
const EDIT_ITEM = 'ducks/agents/EDIT_ITEM';
const ADD_ITEM = 'ducks/agents/ADD_ITEM';

export default function reducer( state = {}, action = {} ){
  switch(action.type) { 
    case DELETE_ITEM:
      return {...state,
          ...{
              transactions : state.agents.filter(function(el,index){
                return index != action.index; 
              })
          }
      }
    default : return state;
   };
};

export function deleteItem(index){
  return { type: DELETE_ITEM, index }
};
export function renameItem(index){
  return { type: EDIT_ITEM, index }
};
export function addItem(data){
  return { type: ADD_ITEM, data }
};