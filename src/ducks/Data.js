require('es6-promise').polyfill();
import fetch from 'isomorphic-fetch'

export const NAVIGATE = 'NAVIGATE';

export const OPEN_DIALOG = 'OPEN_DIALOG';
export const CLOSE_DIALOG = 'CLOSE_DIALOG';

export const TRANSACTION_REQUEST = 'TRANSACTION_REQUEST';
export const TRANSACTION_RECEIVE = 'TRANSACTION_RECEIVE';
export const TRANSACTION_LOAD_ERROR = 'TRANSACTION_LOAD_ERROR';

export const UPDATE_TRANSACTION_IN_DB = 'UPDATE_TRANSACTION_IN_DB';
export const UPDATE_TRANSACTION_IN_DB_ERROR = 'UPDATE_TRANSACTION_IN_DB_ERROR';
export const UPDATE_TRANSACTION_IN_DB_SUCCESS = 'UPDATE_TRANSACTION_IN_DB_SUCCESS';

export const DELETE_TRANSACTION_IN_DB = 'DELETE_TRANSACTION_IN_DB';
export const DELETE_TRANSACTION_IN_DB_ERROR = 'DELETE_TRANSACTION_IN_DB_ERROR';
export const DELETE_TRANSACTION_IN_DB_SUCCESS = 'DELETE_TRANSACTION_IN_DB_SUCCESS';

export const ADD_TRANSACTION_IN_DB = 'ADD_TRANSACTION_IN_DB';
export const ADD_TRANSACTION_IN_DB_ERROR = 'ADD_TRANSACTION_IN_DB_ERROR';
export const ADD_TRANSACTION_IN_DB_SUCCESS = 'ADD_TRANSACTION_IN_DB_SUCCESS';

export const AGENTS_REQUEST = 'AGENTS_REQUEST';
export const AGENTS_RECEIVE = 'AGENTS_RECEIVE';
export const AGENTS_LOAD_ERROR = 'AGENTS_LOAD_ERROR';

export const ADD_AGENT_IN_DB = 'ADD_AGENT_IN_DB';
export const ADD_AGENT_IN_DB_ERROR = 'ADD_AGENT_IN_DB_ERROR';
export const ADD_AGENT_IN_DB_SUCCES = 'ADD_AGENT_IN_DB_SUCCES';

export const DELETE_AGENT_IN_DB = 'DELETE_AGENT_IN_DB';
export const DELETE_AGENT_IN_DB_ERROR = 'DELETE_AGENT_IN_DB_ERROR';
export const DELETE_AGENT_IN_DB_SUCCES = 'DELETE_AGENT_IN_DB_SUCCES';

export const UPDATE_AGENT_IN_DB = 'UPDATE_AGENT_IN_DB';
export const UPDATE_AGENT_IN_DB_ERROR = 'UPDATE_AGENT_IN_DB_ERROR';
export const UPDATE_AGENT_IN_DB_SUCCESS = 'UPDATE_AGENT_IN_DB_SUCCESS';

function sort(a) {
  var temp = {};
  for(var i=1;i<a.length;i++){
    temp = new Object(a[i]);
    var j = i-1;
    console.log();
    while((j>-1)&&(temp.date>a[j].date)){
      a[j+1]= new Object(a[j]);
      j--;
    }
    a[j+1] = new Object(temp);
}
  
return a;
}

export default function reducer(state = {
  transactions: [],
  agents: [],
  dialog : {
      state: false,
      itemId: null,
      submit: false,
      addAgent: false,
      addTransaction: false,
      edit: false,
      error: false,
      editedItem:{}
    },
  view: {
    navigate: 0
  }
  
}, action={} ){
  switch(action.type) {
    case CLOSE_DIALOG:
      return {
        ...state,
        ...{
          dialog: {
            itemId: null,
            submit: false,
            add: false,
            edit: false,
            addTransaction: false,
            addAgent: false
          }
        }
      }
      break;
    case OPEN_DIALOG:
      switch(action.dialogType){
        case 'add': 
          return {
            ...state,
            ...{
              dialog: {
                ...state.dialog,
                ... {
                editedItem: action.payload.data ? action.payload.data : {} ,
                addAgent: action.payload.addAgent,
                addTransaction: action.payload.addTransaction
                }
              }
              }
            }
          break;
        case 'delete' : 
          return {
            ...state,
            ...{
              dialog: {
                ...state.dialog,
                ... {
                itemId: action.payload.index,
                submit: true
                }
              }
            }
          }
          break;
        case 'edit' : 
          return {
            ...state,
            ...{
              dialog: {
                ...state.dialog,
                ... {
                itemId: action.payload.index, 
                editedItem: action.payload.data,
                edit: true
                }
              }
            }
          }
          break;
        default: return 0;
      }
      break;

    case TRANSACTION_REQUEST:
      return {
        ...state,
        ...{
          requests: {
            transactions: 'requested'
          }
        }
      }
      break;
    case TRANSACTION_RECEIVE: 
      return {
        ...state,
        ...{
            transactions: sort(action.data),
            requests:{
              transactions: 'success'
            }
          }
      }
      break;
    case UPDATE_TRANSACTION_IN_DB_ERROR:
      return {
        ...state,
        ...{
          updates: {
            transaction:{
              err: action.payload
            }
          }
        }
      }
      break;
    case UPDATE_TRANSACTION_IN_DB_SUCCESS:
      return {
        ...state,
        ... {
          updates:{
            transaction: {
              state: action.payload
            }
          }
        }
      }
      break;
    case DELETE_TRANSACTION_IN_DB_ERROR:
      return {
        ...state,
        ...{
          updates:{
            transaction: {
              err: action.payload
            }
          }
        }
      }
      break;
    case DELETE_TRANSACTION_IN_DB_SUCCESS:
      return {
        ...state,
        ...{
          updates: {
            transaction: {
              state: action.payload
            }
          }
        }
      }
      break;
    case ADD_TRANSACTION_IN_DB_ERROR: 
      return {
        ...state,
        ...{
          updates: {
            transaction: {
              state: action.paylaod
            }
          }
        }
      }
      break;
    case ADD_TRANSACTION_IN_DB_SUCCESS:
      return {
        ...state,
        ...{
          updates: {
            transaction: {
              state: action.payload
            }
          }
        }
      }
      break;
    case AGENTS_REQUEST:
      return {
        ...state,
        ...{
          requests: {
            agents: 'requested'
          }
        }
      }
      break;
    case AGENTS_RECEIVE:
      return {
        ...state,
        ...{
            agents: action.data,
            requests:{
              agents: 'success'
            }
        }
      }
      break;
    case DELETE_AGENT_IN_DB_ERROR:
      return {
        ...state,
        ...{
          updates:{
            agents:{
              state: action.payload
            }
          }
        }
      }
      break;
    case DELETE_AGENT_IN_DB_SUCCES:
      return {
        ...state,
        ...{
          updates:{
            agents:{
              state: action.payload
            }
          }
        }
      }
      break;
    case NAVIGATE:
      return {
        ...state,
        ...{
          view: {
            navigate: action.payload
          }
        }
      }
      break;
    default: return state;
  }  
}
export function requestAllTransactions(){
  return {
    type : TRANSACTION_REQUEST,
    status : 'Transactions requested'
  }
}
export function receiveAllTransactions(transactions){
  return{ 
    type : TRANSACTION_RECEIVE,
    data : transactions
  }
}
export function fetchAllTransactions(){
  return dispatch => {
    dispatch(requestAllTransactions());
    return fetch('//localhost:3030/transactions')
              .then(response => response.json())
              .then(json => dispatch(receiveAllTransactions(json)))
  } 
}
export function requestAllAgents(){
  return {
    type : AGENTS_REQUEST,
    status : 'Transactions requested'
  }
}
export function receiveAllAgents(agents){
  return{ 
    type : AGENTS_RECEIVE,
    data : agents
  }
}
export function fetchAllAgents(){
  return dispatch => {
    dispatch(requestAllAgents());
    return fetch('//localhost:3030/agents')
              .then(response => response.json())
              .then(json => dispatch(receiveAllAgents(json)))
  } 
}
function updateTransactionInDBErr(err){
  return {
    type: UPDATE_TRANSACTION_IN_DB_ERROR,
    payload: err.message
  }
}
function updateTransactionInDBSuc(dispatch,req){
  console.log(req);
  dispatch(closeDialog());
  dispatch(fetchAllTransactions());
  return {
    type : UPDATE_TRANSACTION_IN_DB_SUCCESS,
    payload : 'Transaction update' 
  }
}
export function updateTransactionInDB(transaction,index){
  transaction.cost = +transaction.cost;
  transaction.agentId = +transaction.agentId;
  console.log('//localhost:3030/transactions/'+(index));
  console.log(transaction, index);
  var options = {
              headers: {
                'Accept' : 'application/json',
                'Content-Type': 'application/json'
                },
              method:'PATCH',
              body:JSON.stringify(transaction)}
  return dispatch => {
    return fetch('//localhost:3030/transactions/'+(index),options)
              .then(req=>dispatch(updateTransactionInDBSuc(dispatch,req)))
              .catch(err=>dispatch(updateTransactionInDBErr(err)));
  } 
}
function deleteTransactionInDBErr(err){
  return {
    type: DELETE_TRANSACTION_IN_DB_ERROR,
    payload: err.message
  }
}
function deleteTransactionInDBSuc(dispatch){
  dispatch(closeDialog());
  dispatch(fetchAllTransactions());
  return {
    type: DELETE_TRANSACTION_IN_DB_SUCCESS,
    payload: 'Transaction deleted'
  }
}
export function deleteTransactionInDB(index){
  var options = {
              headers: {
                'Accept' : 'application/json',
                'Content-Type':'application/x-www-form-urlencoded'
                },
              method:'DELETE'
            }
  return dispatch => {
    return fetch('//localhost:3030/transactions/'+(index),options)
              .then(req=>dispatch(deleteTransactionInDBSuc(dispatch)))
              .catch(err=> dispatch(deleteTransactionInDBErr(err)))
  } 
}
function addTransactionInDBErr(err){
  return {
    type: ADD_TRANSACTION_IN_DB_ERROR,
    payload: err.message
  }
}
function addTransactionInDBSuc(dispatch) {
  dispatch(closeDialog());
  dispatch(fetchAllTransactions());
  return {
    type: ADD_TRANSACTION_IN_DB_SUCCESS,
    payload: 'Transaction added'
  }
}
export function addTransactionInDB(transaction) {
 transaction.date = Date.now();
 transaction.cost = +transaction.cost;
 transaction.agentId = +transaction.agentId;
 
 var options = {
              headers: {
                'Accept' : 'application/json',
                'Content-Type': 'application/json'
                },
              method:'POST',
              body: JSON.stringify(transaction)
            }
  return dispatch => {
    console.log(dispatch);
    return fetch('//localhost:3030/transactions/',options)
              .then(req=>dispatch(addTransactionInDBSuc(dispatch)))
              .catch(err=>dispatch(addTransactionInDBErr(err)));
  } 
}
export function openDialog(options){
  return {
    type: OPEN_DIALOG,
    payload: options,
    dialogType: options.dialogType
  }
}
export function closeDialog(){
  return {
    type: CLOSE_DIALOG
  }
}
export function deleteAgentInDB(index){
  var options = {
              headers: {
                'Accept' : 'application/json',
                'Content-Type':'application/x-www-form-urlencoded'
                },
              method:'DELETE'
            }
  return dispatch => {
    return fetch('//localhost:3030/agents/'+(index),options)
              .then(req=>dispatch(deleteAgentInDBSuc(dispatch)))
              .catch(err=> dispatch(deleteAgentInDBErr(err)))
  } 
}
function deleteAgentInDBErr(err){
  return {
    type: DELETE_AGENT_IN_DB_ERROR,
    payload: err.message
  }
}
function deleteAgentInDBSuc(dispatch){
  dispatch(closeDialog());
  dispatch(fetchAllAgents());
  return {
    type: DELETE_AGENT_IN_DB_SUCCESS,
    payload: 'Agent deleted'
  }
}
function addAgentInDBErr(err){
  return {
    type: ADD_AGENT_IN_DB_ERROR,
    payload: err.message
  }
}
function addAgentInDBSuc(dispatch) {
  dispatch(closeDialog());
  dispatch(fetchAllAgents());
  return {
    type: ADD_AGENT_IN_DB_SUCCESS,
    payload: 'Agent added'
  }
}
export function addAgentInDB(agent) {
 agent.transCount = 0;
 console.log(agent);
 var options = {
              headers: {
                'Accept' : 'application/json',
                'Content-Type': 'application/json'
                },
              method:'POST',
              body: JSON.stringify(agent)
            }
  return dispatch => {
    console.log(dispatch);
    return fetch('//localhost:3030/agents/',options)
              .then(req=>dispatch(addAgentInDBSuc(dispatch)))
              .catch(err=>dispatch(addAgentInDBErr(err)));
  } 
}

function updateAgentInDBErr(err){
  return {
    type: UPDATE_AGENT_IN_DB_ERROR,
    payload: err.message
  }
}
function updateAgentInDBSuc(dispatch,req){
  dispatch(closeDialog());
  dispatch(fetchAllAgents());
  return {
    type : UPDATE_AGENT_IN_DB_SUCCESS,
    payload : 'Agent update' 
  }
}

export function updateAgentInDB(agent,index) {
  var options = {
              headers: {
                'Accept' : 'application/json',
                'Content-Type': 'application/json'
                },
              method:'PATCH',
              body:JSON.stringify(agent)}
  return dispatch => {
    return fetch('//localhost:3030/agents/'+(index),options)
              .then(req=>dispatch(updateAgentInDBSuc(dispatch)))
              .catch(err=>dispatch(updateAgentInDBErr(err)));
  } 
}

export function navigate(num) {
  return {
    type: NAVIGATE,
    payload: num
  }
}