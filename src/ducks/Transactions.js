const DELETE_ITEM = 'ducks/transactions/DELETE_ITEM';
const EDIT_ITEM = 'ducks/transactions/EDIT_ITEM';
const ADD_ITEM = 'ducks/transactions/ADD_ITEM';


export default function reducer( state = {}, action = {} ){
  switch(action.type) {
    case DELETE_ITEM: {
      return {
        ...state,
        ...{
          process: action.payload
        }
      }
    }
      break;
    default : return state;
   };
};

export function deleteItem(index){
  return {
    type: DELETE_ITEM,
    payload: 'Deleted transaction ' + (index) 
  }
};
export function renameItem(index){
  return { type: EDIT_ITEM, index }
};
export function addItem(data){
  return { type: ADD_ITEM, data }
};