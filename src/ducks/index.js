import { combineReducers } from 'redux';
import Transactions from './Transactions';
import Data from './Data';
import Agents from './Agents';
import { reducer as formReducer } from 'redux-form';


const rootReducer = combineReducers(
  {
    Transactions,
    Data,
    Agents,
    form: formReducer
  }
)
export default rootReducer;