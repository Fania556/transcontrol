import React from 'react';
import {render} from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import App from './containers/App';
import thunkMiddleware from 'redux-thunk'
import rootReducer from './ducks';
import injectTapEventPlugin from 'react-tap-event-plugin';
injectTapEventPlugin();


const store = createStore(rootReducer,applyMiddleware(thunkMiddleware));
render(
<Provider store={store}>
    <App/>
</Provider>, document.getElementById('root'));
